$(function(){
  			$('[data-toggle="tooltip"]').tooltip();
  			$('[data-toggle="popover"]').popover();
  			$('.carousel').carousel({
 				 interval: 4000
			})
			$("#contacto").on("show.bs.modal", function(x){
				console.log("el modal se está mostrando");
				$("#contactoBtn").removeClass("btn-outline-success");
				$("#contactoBtn").addClass("btn-secondary");
				$("#contactoBtn").prop('disabled', true);
			})
			$("#contacto").on("shown.bs.modal", function(x){
				console.log("el modal se mostró");
			})
			$("#contacto").on("hide.bs.modal", function(x){
				console.log("el modal se está ocultando");
				$("#contactoBtn").removeClass("btn-secondary");
				$("#contactoBtn").addClass("btn-outline-success");
				$("#contactoBtn").prop('disabled', false);
			})
			$("#contacto").on("hiden.bs.modal", function(x){
				console.log("el modal se ocultó");
			})
		});